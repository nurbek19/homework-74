const express = require('express');
const messages = require('./app/messages');
const fileDb = require('./fileDb');
const app = express();

const port = 8000;

app.use(express.json());


app.use('/messages', messages(fileDb));

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});
