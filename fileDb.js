const fs = require('fs');

module.exports = {
    getMessages: () => {

        return new Promise((resolve, reject) => {
            const path = "./messages";

            fs.readdir(path, (err, files) => {
                const messages = [];

                if (err) {
                    reject(err);
                } else {
                    files.slice(-5).forEach(file => {
                        const fileName = path + '/' + file;

                        const promise = new Promise((resolve, reject) => {
                            fs.readFile(fileName, 'utf8', (err, res) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(res);
                                }
                            });
                        });

                        messages.push(promise);
                    });

                    resolve(messages);
                }
            });
        });
    },

    addMessage: message => {
        const datetime = new Date().toISOString();
        const fileName = `./messages/${datetime}`;
        const content = message;
        message.datetime = datetime;

        return new Promise((resolve, reject) => {
            fs.writeFile(fileName, JSON.stringify(content), (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(content);
                }
            });
        });
    }
};