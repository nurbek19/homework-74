const express = require('express');
const router = express.Router();


const createRouter = db => {

    router.get('/', (req, res) => {
        db.getMessages().then(promises => {
            Promise.all(promises).then(result => {
                const messages = result.map(item => JSON.parse(item));
                res.send(messages);
            });
        })
    });

    router.post('/', (req, res) => {
        const message = req.body;

        db.addMessage(message).then(result => {
            res.send(JSON.stringify(result));
        });
    });

    return router;
};

module.exports = createRouter;